<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon"
	href="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU"
	type="image/gif" sizes="16x16">
<title>Register</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
<
script>body {
	background: #333;
}

div {
	background: linear-gradient(to bottom, #33ccff 0%, #ff99cc 100%);
	height: 100%;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}

#login {
	-webkit-perspective: 1000px;
	-moz-perspective: 1000px;
	perspective: 1000px;
	margin-top: 50px;
	margin-left: 30%;
}

.login {
	font-family: 'Josefin Sans', sans-serif;
	-webkit-transition: .3s;
	-moz-transition: .3s;
	transition: .3s;
	-webkit-transform: rotateY(40deg);
	-moz-transform: rotateY(40deg);
	transform: rotateY(40deg);
}

.login:hover {
	-webkit-transform: rotate(0);
	-moz-transform: rotate(0);
	transform: rotate(0);
}

.login article {
	
}

.login .form-group {
	margin-bottom: 17px;
}

.login .form-control, .login .btn {
	border-radius: 0;
}

.login .btn {
	text-transform: uppercase;
	letter-spacing: 3px;
}

.input-group-addon {
	border-radius: 0;
	color: #fff;
	background: #f3aa0c;
	border: #f3aa0c;
}

.forgot {
	font-size: 16px;
}

.forgot a {
	color: #333;
}

.forgot a:hover {
	color: #5cb85c;
}

#inner-wrapper, #contact-us .contact-form, #contact-us .our-address {
	color: #1d1d1d;
	font-size: 19px;
	line-height: 1.7em;
	font-weight: 300;
	padding: 50px;
	background: #fff;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0
		rgba(0, 0, 0, 0.12);
	margin-bottom: 100px;
}

.input-group-addon {
	border-radius: 0;
	border-top-right-radius: 0px;
	border-bottom-right-radius: 0px;
	color: #fff;
	background: #f3aa0c;
	border: #f3aa0c;
	border-right-color: rgb(243, 170, 12);
	border-right-style: none;
	border-right-width: medium;
}
</
script
>
</style>
</head>

<body>
	<%
		long number = 0l;
		Random rand = new Random();
		number = (rand.nextInt(1000000) + 1000000000l) * (rand.nextInt(900) + 100);
		System.out.println(number);
		request.setAttribute("actno", number);
	%>
	<center>
		<h1>Register form</h1>
	</center>
	<div>
		<div class="col-md-4 col-md-offset-4" id="login">
			<section id="inner-wrapper" class="login"> <article>
			<form action="RegisterServ" method="post">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user">
						</i></span> <input type="text" class="form-control" name="fname"
							placeholder="fname" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope">
						</i></span> <input type="text" class="form-control" name="lname"
							placeholder="lname">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"> </i></span>
						<input type="date" class="form-control" name="bdate" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"> </i></span>
						<input type="text" class="form-control" name="userid"
							placeholder="username" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"> </i></span>
						<input  type="password" type="number" class="form-control" name="pword"
							minlength="6" maxlength="6" 
							placeholder="password" required><!-- pattern="[1-9]" -->
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"> </i></span>
						<input type="text" class="form-control" name="actno"
							value="<%=number%>" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"> </i></span>
						<input type="radio" class="form-control" name="gen" value="male">
						male <input type="radio" class="form-control" name="gen"
							value="female">female
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"> </i></span>
						<input type="text" class="form-control" name="bal"
							placeholder="balance" required>
					</div>
				</div>
				<button type="submit" class="btn btn-success btn-block">Submit</button>
			</form>
			</article> </section>
		</div>
</body>
</div>
</html>
