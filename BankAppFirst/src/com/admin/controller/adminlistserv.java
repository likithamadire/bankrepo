package com.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/adminlistserv")
public class adminlistserv extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NumberFormatException {
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<title>List</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");
		try {
			String opt1 = request.getParameter("option");
			// int opt1 = Integer.parseInt(option1);

			if (opt1.equals("Delete")) {
				pw.print("Delete");
				response.sendRedirect("admindelete.html");
			} else if (opt1.equals("Details")) {
				pw.print("Details");
				response.sendRedirect("display.jsp");
				/* response.sendRedirect("viewtransactions.jsp"); */
			} else if (opt1.equals("TransactionDetails")) {
				pw.print("TransactionDetails");
				/*
				 * RequestDispatcher dispatcher =
				 * request.getRequestDispatcher("ViewTransByAdmin");
				 */
				response.sendRedirect("TransactionDetails.html");
				/* response.sendRedirect("viewtransactions.jsp"); */
			} else if (opt1.equals("Contact")) {
				pw.print("Contact");
				response.sendRedirect("admincontact");
			}

			else if (opt1.equals("Logout")) {
				response.sendRedirect("bankhomepage.html");
			}
			/*
			 * if (opt1.equals(" ")) { response.sendRedirect("adminlist.html"); } if (opt1
			 * != opt1.equals("Delete") && opt1 != opt1.equals("Details") && opt1 !=
			 * opt1.equals("TransactionDetails") && opt1 != opt1.equals("Contact") && opt1
			 * != opt1.equals("Logout")) { response.sendRedirect("adminlist.html"); }
			 */
			else {

				response.sendRedirect("adminlist.html");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}