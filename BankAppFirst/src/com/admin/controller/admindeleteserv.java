package com.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/admindeleteserv")
public class admindeleteserv extends HttpServlet {
	Connection con;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		String actno = request.getParameter("actno");
		PrintWriter pw = response.getWriter();
		pw.println("<title>Delete Account</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");
		pw.println("<strong> <a href=http://localhost:8090/webank/adminlist.html>Home</a></strong>\t\t");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully");

			PreparedStatement pstmt2 = con.prepareStatement("delete from bankcomplaint where act_no=?");

			pstmt2.setString(1, actno);

			int s2 = pstmt2.executeUpdate();
			
			PreparedStatement pstmt1 = con.prepareStatement("delete from banktransaction where act_no=?");

			pstmt1.setString(1, actno);

			int s1 = pstmt1.executeUpdate();

			PreparedStatement pstmt = con.prepareStatement("delete from bankcustomer where act_no=?");

			pstmt.setString(1, actno);

			int s = pstmt.executeUpdate();

			if (s > 0 && s1 > 0 && s2 > 0) {

				pw.print("<h3>you are successfully deleted</h3>");

				RequestDispatcher rd = request.getRequestDispatcher("adminlist.html");

				rd.forward(request, response);

				// response.sendRedirect("adminlist.html");
			}

			else {

				pw.print("<h3>invalid account number</h3>");

				RequestDispatcher rd = request.getRequestDispatcher("admindelete.html");

				rd.include(request, response);

			}

		} catch (Exception e) {
			System.err.println(e);
		}
	}

}
