package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UpdatePassword")
public class UpdatePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully in user credentials change servlet");
			String userpassword = request.getParameter("newpword");

			Cookie c[] = request.getCookies();
			response.setContentType("text/html");

			int pwdlen = userpassword.length();

			PrintWriter pw = response.getWriter();
			PreparedStatement pstmt;
			if (pwdlen == 0) {
				/*
				 * PreparedStatement pstmt =
				 * con.prepareStatement("update bankcustomer set pword=? where act_no=?");
				 */
				pw.print("Your Credentials are not updated ..Please use Old credentials for next use...!!");
			} else if (pwdlen != 0) {
				pstmt = con.prepareStatement("update bankcustomer set pword=? where act_no=?");
				pstmt.setString(1, userpassword);

				pstmt.setString(2, c[2].getValue());

				pstmt.executeUpdate();

				pw.println("<center><br /> Your password updated successfully</center>");
				RequestDispatcher rd = request.getRequestDispatcher("CredentialHome.html");
				rd.include(request, response);
			}

			/*
			 * if (!userdupl) {
			 * 
			 * pstmt =
			 * con.prepareStatement("update bankcustomer set user_id=? where act_no=?");
			 * pstmt.setString(1, usname); pstmt.setString(2, c[2].getValue());
			 * 
			 * pstmt.executeUpdate();
			 * 
			 * pw.println("<center><br /> Your user id updated successfully</center>");
			 * 
			 * }
			 */

			else {

				pw.println("<center>Given user id is already in use. Please use another user id</center>");
			}
		}

		catch (Exception e) {
			System.err.println(e);

		}

	}

}
