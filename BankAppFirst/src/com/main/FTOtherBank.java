package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/FTOtherBank")
public class FTOtherBank extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Connection con;

	public void init(ServletConfig config) throws ServletException {

		super.init(config);

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully in Funds transfer to Other bank servlet");

		}

		catch (Exception e) {
			System.err.println(e);

		}

	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String s1 = request.getParameter("actno");
		String s2 = request.getParameter("amt");
		String s3 = request.getParameter("ifsc");
		String s4 = request.getParameter("bank");
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<title>Fund Transfer</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");

		pw.println(
				"<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
		pw.print(" <table align=center cellpadding=10 cellspacing=10 border=1> <tr>");
		pw.println("<th><center><strong><a href=WelcomeServlet>Home</a></strong></center></th>");
		pw.println("<th><center><strong><a href=LogOut>Log out</a></strong></center></th>");

		pw.print("</tr></table>");

		pw.println("Recipient Account number is : " + s1);
		pw.println("<br />Balance to be transferred is : " + s2);
		Cookie c[] = request.getCookies();

		PreparedStatement pstmt;

		ResultSet rs1;

		long tranid = 0;
		String remarks = " ", transtatus = " ", trandesc = "Funds Transfer to " + s4 + " Bank", actno = c[2].getValue();

		try {
			pstmt = con.prepareStatement("select count(*) from banktransaction");
			rs1 = pstmt.executeQuery();
			System.out.println(rs1);
			rs1.next();
			tranid = rs1.getLong(1);
			
			
			

		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		
		
		if (Long.parseLong(c[3].getValue()) < Long.parseLong(s2)) {

			pw.println(
					"<br />Funds transfer can not be initiated as available balance is less than the to be transferred amount and <a href=Ftother.html><i><strong>TRY AGAIN</strong></i></a>");
			remarks = "Funds transfer can not be initiated as available balance is less than the to be transferred amount";
			transtatus = "fail";
		}

		else {

			try {
				
				PreparedStatement pstmt1 = con.prepareStatement("select * from bankcustomer where act_no=?");

				pstmt1.setString(1,  s1);

				ResultSet rs = pstmt1.executeQuery();
				if(rs.next()) {

				long newBal = Long.parseLong(c[3].getValue()) - Long.parseLong(s2);

				pstmt = con.prepareStatement("update bankcustomer set balance=? where act_no=?");

				pstmt.setLong(1, newBal);
				pstmt.setString(2, actno);

				pstmt.executeUpdate();

				pw.println("<br /> Funds Rs " + s2 + " transfer initiated to the account number " + s1);

				pw.println("<br />Available balance in the account is Rs " + newBal);

				remarks = "Funds transferred successfully";
				transtatus = "pass";
				}else {
					pw.println("<br /><br /><strong>Recipient account number is incorrect. Please check <a href=Ftother.html><i><strong>TRY AGAIN</strong></i></a></strong>");
					remarks = "Recipient account number is incorrect";
					transtatus = "fail";
				}
			}

			catch (Exception e) {

				e.printStackTrace();
			}

			System.out.println("New balance is updated successfully in database");

		}

		try {
			pstmt = con.prepareStatement(
					"insert into banktransaction(tranid,act_no,tran_desc,tran_status,remarks,ifsc,toact_no,amount) values(?,?,?,?,?,?,?,?)");

			pstmt.setLong(1, tranid);
			pstmt.setString(2, actno);
			pstmt.setString(3, trandesc);
			pstmt.setString(4, transtatus);
			pstmt.setString(5, remarks);
			pstmt.setString(6, s3);
			pstmt.setString(7, s1);
			pstmt.setString(8, s2);
			pstmt.executeUpdate();

			PreparedStatement pstmt1 = con.prepareStatement(
					"insert into banktransactionbackup(tranid,act_no,tran_desc,tran_status,remarks,ifsc,toact_no,amount) values(?,?,?,?,?,?,?,?)");

			pstmt1.setLong(1, tranid);
			pstmt1.setString(2, actno);
			pstmt1.setString(3, trandesc);
			pstmt1.setString(4, transtatus);
			pstmt1.setString(5, remarks);
			pstmt1.setString(6, s3);
			pstmt1.setString(7, s1);
			pstmt1.setString(8, s2);

			pstmt1.executeUpdate();

			System.out.println("Transaction table updated successfully");

		} catch (Exception e) {

		}
	}

}
