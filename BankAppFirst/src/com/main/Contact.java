package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Contact")
public class Contact extends HttpServlet {
	Connection con;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		pw.println("<title>Contact us</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");
		try {
			String s1 = request.getParameter("name");
			String s2 = request.getParameter("email");
			String s3 = request.getParameter("message");

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully in retrieving complaints from Database");
			pw.print(" <table align=center cellpadding=10 cellspacing=10 border=1> <tr>");
			pw.println("<th><center><strong><a href=WelcomeServlet>Home</a></strong></center></th>");
			pw.println("<th><center><strong><a href=LogOut>Log out</a></strong></center></th>");

			pw.print("</tr></table>");
			Statement st1 = con.createStatement();
			ResultSet k = st1.executeQuery("select count(*) from bankcontact");
			k.next();
			int slno = k.getInt(1) + 1;
			PreparedStatement pstmt = con.prepareStatement("insert into bankcontact values(?,?,?,?)");

			pstmt.setInt(1, slno);
			pstmt.setString(2, s1);
			pstmt.setString(3, s2);
			pstmt.setString(4, s3);
			pstmt.executeUpdate();
			/* con.commit(); */

			response.sendRedirect("bankhomepage.html");
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
