package com.main;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/LogOut")
public class LogOut extends HttpServlet {
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession hs=request.getSession();
		response.setContentType("text/html");
		PrintWriter pw =response.getWriter();
		pw.println("<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
		
		hs.invalidate();

		pw.println("You have logged out successfully<br />");
		
		ServletContext sc=getServletContext();
		
		//RequestDispatcher rd=sc.getRequestDispatcher("bankhomepage.html");
		
		//rd.include(request, response);
		response.sendRedirect("bankhomepage.html");
	}

}
