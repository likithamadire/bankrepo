package com.main;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ValidateOther
 */
@WebServlet("/ValidateOther")
public class ValidateOther extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
          response.setContentType("text/html");
		try {
		    Class.forName("com.mysql.cj.jdbc.Driver");
		   Connection connection =DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");
		   String s2=request.getParameter("pword");
			System.out.println(s2);
			PreparedStatement pstmt=connection.prepareStatement("select * from bankcustomer where pword=?");
			pstmt.setString(1, s2);
	
		  ResultSet rs=pstmt.executeQuery();
		  PrintWriter pw=response.getWriter();
		  pw.println("<title>Validate</title>");
			pw.println("<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");
			
		  if(rs.next()){
			((HttpServletResponse) response).sendRedirect("Ftother.html");
		  }
		  else{
			  pw.println("invalid password");
			  RequestDispatcher rd=request.getRequestDispatcher("validateother.html");
			  rd.include(request, response);
		  }
		   
		}
		catch(Exception e){
			
		}
	}
       
}
