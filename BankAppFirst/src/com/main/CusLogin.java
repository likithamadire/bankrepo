package com.main;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.model.bank.CusGetSet;

@WebServlet("/CusLogin")
public class CusLogin extends HttpServlet {
	//public CusGetSet cgs;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");
		String s1=request.getParameter("uname");
		String s2=request.getParameter("pword");
		
		PreparedStatement pstmt=con.prepareStatement("select * from bankcustomer where user_id=? and pword=?");
		pstmt.setString(1, s1);
		pstmt.setString(2, s2);
		
		ResultSet rs=pstmt.executeQuery();
		
		PrintWriter pw=response.getWriter();
		
		if(rs.next()){
			HttpSession hs=request.getSession();
			hs.setAttribute("uname",s1);
			hs.setAttribute("pword", s2);
			 /*cgs=new CusGetSet();
			cgs.setPword(s2);*/
		/*
			RequestDispatcher rd=request.getRequestDispatcher("WelcomeServlet");
			rd.forward(request, response);*/
		   response.sendRedirect("WelcomeServlet");
				
		}
		else{
			RequestDispatcher rd=request.getRequestDispatcher("login.html");
			rd.include(request, response);
		}
		
	} 
	catch (ClassNotFoundException e) {
		e.printStackTrace();
	} 
	catch (SQLException e) {
		e.printStackTrace();
	}
	}

}
