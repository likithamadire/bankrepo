package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/WelcomeServlet")
public class WelcomeServlet extends HttpServlet {

	Connection con;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// String s1=request.getParameter("uname");
		// String s2=request.getParameter("pword");

		System.out.print("web bank");
		HttpSession hs = request.getSession();

		String un = (String) hs.getAttribute("uname");
		String pwd = (String) hs.getAttribute("pword");

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully in Customer welcome servlet");

			PreparedStatement pstmt = con.prepareStatement("select * from bankcustomer where user_id=? and pword=?");

			if (!un.equals(null) && !pwd.equals(null)) {
				pstmt.setString(1, un);
				pstmt.setString(2, pwd);
			}

			PrintWriter pw = response.getWriter();

			ResultSet rs = pstmt.executeQuery();

			rs.next();

			Cookie c1 = new Cookie("lname", rs.getString(2));
			Cookie c2 = new Cookie("AccountNo", rs.getString(6));
			Cookie c3 = new Cookie("bal", rs.getString(8));

			response.addCookie(c1);
			response.addCookie(c2);
			response.addCookie(c3);

			response.setContentType("text/html");
			pw.println("<title>Welcome " + rs.getString(2) + " </title>");
			pw.println(
					"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");
			pw.println(
					"<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
			pw.print(" <table align=center cellpadding=10 cellspacing=10 border=1> <tr>");

			pw.println("<th><strong><a href=contactus.html>Contact Us</a></strong> </th>");
			pw.println("<th><strong><a href=CredentialHome.html >Change User Credentials</a></strong></th>");
			pw.println("<th><strong><a href=LogOut>Log out</a></strong> </th>");

			pw.println("</table></tr>");

			pw.println("<hr />");
			pw.println("<html> <h4>Welcome <em>" + rs.getString(2) + "</em></h4>");

			pw.println("Account Number  : <strong>" + rs.getString(6) + "</strong>");
			pw.println("<br />Your current Balance  : <strong> Rs " + rs.getString(8) + "</strong>");

			pw.println("<h3>Menu</h3>");

			pw.print(" <table align=center cellpadding=5 cellspacing=8 border=2> <tr>");

			pw.println(
					"<th> <a href=CheckBalance><img src='https://www.google.com/imgres?imgurl=https%3A%2F%2Fscripbox.com%2Fmf%2Fwp-content%2Fuploads%2F2020%2F12%2Fmy-account-balance-1200x1200.jpg&imgrefurl=https%3A%2F%2Fscripbox.com%2Fmf%2Fmy-account-balance%2F&tbnid=RvWPV2hVQdrhLM&vet=12ahUKEwjBpOK6vLbvAhWNwzgGHbt4BkMQMygJegUIARDOAQ..i&docid=oEi_3ngDvuDYdM&w=1200&h=1200&q=check%20balance%20image&ved=2ahUKEwjBpOK6vLbvAhWNwzgGHbt4BkMQMygJegUIARDOAQ'></a>Check balance</li></th>");

			pw.println(
					" <th> <a href=validate.html><img src='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTLs0EmTIijYarodi9aNUWC-eipCnFYm6zs2g&usqp=CAU'></a>with in bank</li></th>");

			/*
			 * pw.println(
			 * "<br /> <li> <a href=Deposite.jsp><img src='https://www.investopedia.com/thmb/L7wSHU4xwWu_PGe4GqZ6gPMC9jM=/1651x1238/smart/filters:no_upscale()/bank-4541449_1920-9d09a6bbf7bb47b6bb77a81188383f8f.jpg'style='width:341px;height:148px;'></a>Deposite Money</li>"
			 * );
			 */

			pw.println(
					"<th> <a href=validateother.html><img src='https://www.investopedia.com/thmb/L7wSHU4xwWu_PGe4GqZ6gPMC9jM=/1651x1238/smart/filters:no_upscale()/bank-4541449_1920-9d09a6bbf7bb47b6bb77a81188383f8f.jpg'style='width:341px;height:148px;'></a>other bank</th></tr>");
			pw.println(
					"<tr><th><a href=complaint.html><img src='https://blog.ipleaders.in/wp-content/uploads/2016/08/complaint.jpg'style='width:341px;height:148px;'></a>complaint</li></th>");
			pw.println(
					"<th> <a href=ComplaintBase><img src='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR8g0w530clMvvFo68lbmJBrwckmlx_RUEZmQ&usqp=CAU'style='width:341px;height:148px;'></a>complaint status</th>");
			pw.println(
					"<th> <a href=ViewTrans><img src='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTRuozihAqLeH2r3BfkeuP1GL6wRTvQZmsMzQ&usqp=CAU'style='width:341px;height:148px;'></a>Statement</th></tr>");
			pw.println(
					"<tr><th><a href=CredentialHome.html><img src='https://kinsta.com/wp-content/uploads/2020/06/change-wordpress-password-1024x512.jpg' style='width:341px;height:148px;'></a>change credentials</th>");
			pw.println(
					"<th><a href=LogOut><img src='https://images.assetsdelivery.com/compings_v2/ahasoft2000/ahasoft20001908/ahasoft2000190802871.jpg'style='width:341px;height:148px;'></a>logout</ul></th>	</tr>");

		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
