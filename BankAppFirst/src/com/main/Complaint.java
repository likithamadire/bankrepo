package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Complaint")
public class Complaint extends HttpServlet {

	Connection con;
	long complNo;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NullPointerException {

		String s1 = request.getParameter("subject");
		String s2 = request.getParameter("desc");

		Cookie c[] = request.getCookies();

		response.setContentType("text/html");
		PreparedStatement pstmt;

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully in registering complaint servlet");

			Statement st1 = con.createStatement();
			ResultSet k = st1.executeQuery("select count(*) from bankcomplaintbackup");
			k.next();
			int complNo = k.getInt(1) + 1;
			// System.out.println(count1);
			// complNo++;
			pstmt = con.prepareStatement(
					"insert into bankcomplaint(complaint_no,act_no,subject,description,status) values(?,?,?,?,?)");

			System.out.println("complaint No is :" + complNo);
			pstmt.setLong(1, complNo);
			pstmt.setString(2, c[2].getValue());
			pstmt.setString(3, s1);
			pstmt.setString(4, s2);
			pstmt.setString(5, "register");
			pstmt.executeUpdate();

			PreparedStatement pstmt1 = con.prepareStatement(
					"insert into bankcomplaintbackup(complaint_no,act_no,subject,description,status) values(?,?,?,?,?)");

			System.out.println("complaint No is :" + complNo);
			pstmt1.setLong(1, complNo);
			pstmt1.setString(2, c[2].getValue());
			pstmt1.setString(3, s1);
			pstmt1.setString(4, s2);
			pstmt1.setString(5, "register");
			pstmt1.executeUpdate();
			System.out.println("added to bankcomplaintbackup table");
			PrintWriter pw = response.getWriter();

			/* PrintWriter pw=response.getWriter(); */
			pw.println("<title>Complaint Register</title>");
			pw.println(
					"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");
			pw.println(
					"<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
			pw.println("<a href=WelcomeServlet>Home</a>\t\t");

			pw.println("<a href=LogOut>Log out</a><br /><br />");

			pw.println("<br /><center><strong>Your complaint is registered successfully and the complaint number is " + complNo+"</center></strong>");
		}

		catch (Exception e) {
			System.err.println(e);
		}

	}

}
