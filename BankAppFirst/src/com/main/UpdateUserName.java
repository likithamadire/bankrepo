package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UpdateUserName")
public class UpdateUserName extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully in user credentials change servlet");
			String usname = request.getParameter("newuid");

			Cookie c[] = request.getCookies();
			response.setContentType("text/html");

			int uidlen = usname.length();

			PrintWriter pw = response.getWriter();
			if (uidlen == 0) {
				/*
				 * PreparedStatement pstmt =
				 * con.prepareStatement("update bankcustomer set pword=? where act_no=?");
				 */
				pw.print("Your Credentials are not updated ..Please use Old credentials for next use...!!");
			} else if (uidlen != 0) {

				PreparedStatement pstmt = con.prepareStatement("select user_id from bankcustomer");
				ResultSet rs = pstmt.executeQuery();
				boolean userdupl = false;
				while (rs.next()) {

					if (usname.equals(rs.getString(1)))
						userdupl = true;

				}

				if (!userdupl) {

					pstmt = con.prepareStatement("update bankcustomer set user_id=? where act_no=?");
					pstmt.setString(1, usname);
					pstmt.setString(2, c[2].getValue());

					pstmt.executeUpdate();

					pw.println("<center><br /> Your user id updated successfully</center>");
					RequestDispatcher rd=request.getRequestDispatcher("CredentialHome.html");
			           rd.include(request, response);

				}

				else {

					pw.println("<center>Given user id is already in use. Please use another user id</center>");
				}
			}
		}

		catch (Exception e) {
			System.err.println(e);

		}

	}

}
