package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.model.bank.CusGetSet;

@WebServlet("/RegisterServ")
public class RegisterServ extends HttpServlet {

	String var;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<title>Register</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");

		try {
			String s1 = request.getParameter("fname");
			String s2 = request.getParameter("lname");
			String s3 = request.getParameter("bdate");
			String s4 = request.getParameter("userid");
			String s5 = request.getParameter("pword");
			String s6 = request.getParameter("actno");
			String s7 = request.getParameter("gen");
			String s8 = request.getParameter("bal");
			System.out.println(s3);
			/* String s3 = "2001/03/17"; */
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date d = sdf.parse(s3);
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH) + 1;
			int date = c.get(Calendar.DATE);
			LocalDate l1 = LocalDate.of(year, month, date);
			LocalDate now1 = LocalDate.now();
			Period diff1 = Period.between(l1, now1);
			System.out.println("age:" + diff1.getYears() + "years");
			if (diff1.getYears() >= 18) {
				var = "Major";
			} else {
				var = "Minor";
			}
			System.out.println(s1 + " " + s2 + " " + s3 + " " + s4 + " " + s5 + " " + s6 + " " + s7 + " " + s8);
			CusGetSet cgs = new CusGetSet();
			cgs.setFname(s1);
			cgs.setLname(s2);
			cgs.setBdate(s3);
			cgs.setUserid(s4);
			cgs.setPword(s5);
			cgs.setActno(s6);
			cgs.setGender(s7);
			cgs.setBal(s8);

			System.out.println("hello");
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("hello1");

			PreparedStatement pstmt = con.prepareStatement("insert into bankcustomer values(?,?,?,?,?,?,?,?,?)");

			pstmt.setString(1, cgs.getFname());
			pstmt.setString(2, cgs.getLname());
			pstmt.setString(3, cgs.getBdate());
			pstmt.setString(4, cgs.getUserid());
			pstmt.setString(5, cgs.getPword());
			pstmt.setString(6, cgs.getActno());
			pstmt.setString(7, cgs.getGender());
			pstmt.setString(8, cgs.getBal());
			pstmt.setString(9, var);
			System.out.println(var);
			int result = pstmt.executeUpdate();

			PreparedStatement pstmt1 = con.prepareStatement("insert into bankaccountbackup values(?)");

			pstmt1.setString(1, cgs.getActno());
			int result1 = pstmt1.executeUpdate();
			if (result > 0 && result1 > 0) {
				// con.commit();
				pw.println("data saved successfully");
				RequestDispatcher rd = request.getRequestDispatcher("login.html");
				rd.include(request, response);
			} else {

				pw.print("You are not registered ... please try again ..");
				response.sendRedirect("register.jsp");

			}

		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (ParseException e) {
			
			e.printStackTrace();
		}

	}

}
