package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UpdateCredentials")
public class UpdateCredentials extends HttpServlet {

	Connection con;

	public void init(ServletConfig config) throws ServletException {

		super.init(config);

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");
			System.out.println("Database connection established successfully in user credentials change servlet");

		}

		catch (Exception e) {
			System.err.println(e);

		}

	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

		String s1 = request.getParameter("newuid");
		String s2 = request.getParameter("newpword");
		Cookie c[] = request.getCookies();
		response.setContentType("text/html");

		int uidlen = s1.length();
		int pwdlen = s2.length();

		PrintWriter pw = response.getWriter();
		pw.println("<title>Update Credentials</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");

		pw.println(
				"<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
		pw.print(" <table align=center cellpadding=10 cellspacing=10 border=1> <tr>");
		pw.println("<th><center><strong><a href=WelcomeServlet>Home</a></strong></center></th>");
		pw.println("<th><center><strong><a href=LogOut>Log out</a></strong></center></th>");

		pw.print("</tr></table>");

		

		PreparedStatement pstmt;

		try {

			/*if (uidlen == 0 && pwdlen == 0) {
				pw.println(
						"<center><br /> You have not updated User Id & Password. Login credentials remain same</center>");
			}

			else if (uidlen == 0 && pwdlen != 0) {
				pstmt = con.prepareStatement("update bankcustomer set pword=? where act_no=?");
				pstmt.setString(1, s2);

				pstmt.setString(2, c[2].getValue());

				pstmt.executeUpdate();

				pw.println("<center><br /> Your password updated successfully</center>");
			}

			else if (uidlen != 0 && pwdlen == 0) {

				pstmt = con.prepareStatement("select user_id from bankcustomer");
				ResultSet rs = pstmt.executeQuery();
				boolean userdupl = false;
				while (rs.next()) {

					if (s1.equals(rs.getString(1)))
						userdupl = true;

				}

				if (!userdupl) {

					pstmt = con.prepareStatement("update bankcustomer set user_id=? where act_no=?");
					pstmt.setString(1, s1);
					pstmt.setString(2, c[2].getValue());

					pstmt.executeUpdate();

					pw.println("<center><br /> Your user id updated successfully</center>");

				}

				else {

					pw.println("<center>Given user id is already in use. Please use another user id</center>");
				}
			}
*/
			if (uidlen != 0 && pwdlen != 0) {

				pstmt = con.prepareStatement("select user_id from bankcustomer");
				ResultSet rs = pstmt.executeQuery();
				boolean userdupl = false;
				while (rs.next()) {

					if (s1.equals(rs.getString(1)))
						userdupl = true;

				}

				if (!userdupl) {
					pstmt = con.prepareStatement("update bankcustomer set user_id=? where act_no=?");
					pstmt.setString(1, s1);
					pstmt.setString(2, c[2].getValue());

					pstmt.executeUpdate();

					pstmt = con.prepareStatement("update bankcustomer set pword=? where act_no=?");
					pstmt.setString(1, s2);
					pstmt.setString(2, c[2].getValue());

					pstmt.executeUpdate();

					pw.println("<center><br /> Your User id & Password updated successfully</center>");
					
					/*RequestDispatcher rd=request.getRequestDispatcher("WelcomeServlet.java");
					rd.include(request, response);*/
				}

				else {
					pw.println("<center>Given user id is already in use. Please use another user id</center>");
				}

			}

		}

		catch (Exception e) {
			System.err.println(e);
		}

	}

}
