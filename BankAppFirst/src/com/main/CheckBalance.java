package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CheckBalance")
public class CheckBalance extends HttpServlet {

	Connection con;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PreparedStatement pstmt;
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<title>Statement</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");

		pw.println(
				"<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
		pw.print(" <table align=center cellpadding=10 cellspacing=10 border=1> <tr>");
		pw.println("<th><center><strong><a href=WelcomeServlet>Home</a></strong></center></th>");
		pw.println("<th><center><strong><a href=LogOut>Log out</a></strong></center></th>");

		pw.print("</tr></table>");
		pw.println("<center><h4>Details of Transactions :</h4></center>");

		Cookie c[] = request.getCookies();

		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			System.out.println("Database connection established successfully");

			pstmt = con.prepareStatement("select balance from bankcustomer where act_no=?",
					ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			pstmt.setString(1, c[2].getValue());

			ResultSet rs = pstmt.executeQuery();
			/* (rs.absolute(1)) */
			if (rs.next()) {

				ResultSetMetaData rm = rs.getMetaData();

				int colcnt = rm.getColumnCount();
				pw.print(" <table align=center cellpadding=10 cellspacing=10 border=2 > <tr>");
				for (int i = 1; i <= colcnt; i++) {

					pw.print(" <th>" + rm.getColumnName(i) + "</th>");
					/*
					 * pw.print("<strong>" +"  "+ rm.getColumnName(i).toLowerCase()
					 * +"    "+"</strong>");
					 */

				}
			/*	pw.print("</tr>");*/

				rs.first();
				do {
					pw.println("<br />");
					pw.print("<tr>");
					for (int i = 1; i <= colcnt; i++) {
						/*pw.print("hello");*/
						pw.print("<td>" + rs.getString(i) + "</td>");

					}

				

				} while (rs.next());
				/*pw.println(
						" <center> <h1><button type=\"button\" onclick=\"window.print()\">print</button> </h1></center>");
			}
			
			else {
				pw.println("<center>There are no Transactions happened in this account</center>");
			}*/
			pw.print("</tr></table>");
		}
		}

		catch (Exception e) {
			System.err.println(e);
		}

	}

}
