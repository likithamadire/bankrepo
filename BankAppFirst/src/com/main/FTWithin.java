package com.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/FTWithin")
public class FTWithin extends HttpServlet {

	/*
	 * public FTWithin() { super(); }
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NumberFormatException {

		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();

		String s1 = request.getParameter("actno");
		String s2 = request.getParameter("amt");
		String s3 = request.getParameter("ifsc");

		pw.println("<title>Fund Transfer</title>");
		pw.println(
				"<link rel='icon' href='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSobgGygd2MoDxxn1PKrU9av2Y8ZLAh6imiXg&usqp=CAU' type='image/gif' sizes='16x16'>");

		pw.println(
				"<body background='https://camo.githubusercontent.com/161566499195bcae38b872e7b34ec18e9ff52180/687474703a2f2f692e696d6775722e636f6d2f744735644948782e6a7067'>");
		pw.print(" <table align=center cellpadding=10 cellspacing=10 border=1> <tr>");
		pw.println("<th><center><strong><a href=WelcomeServlet>Home</a></strong></center></th>");
		pw.println("<th><center><strong><a href=LogOut>Log out</a></strong></center></th>");

		pw.print("</tr></table>");

		pw.println("<h3>Funds Transfer to Other Account With in Bank :</h3> ");
		pw.println("Recipient Account number is : " + s1 + "<br />");
		pw.println("Balance to be transferred is : " + s2);

		Cookie c[] = request.getCookies();
		PreparedStatement pstmt;
		ResultSet rs1;
		long tranid = 0;
		String remarks = "", transtatus = "", trandesc = "Funds Transfer With in the Bank", actno = c[2].getValue();
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
			// pstmt =con.prepareStatement("select tran_seq.nextval from dual");
			pstmt = con.prepareStatement("select count(*) from banktransaction");
			rs1 = pstmt.executeQuery();
			System.out.println(rs1);
			rs1.next();
			tranid = rs1.getLong(1);

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (actno == s1) {
			pw.print(
					"transaction cannot be done ...<br><strong>You are trying to do transaction to your bank account!! which is not possible!!!!</strong>");
		}
		else {
		if (Integer.parseInt(c[3].getValue()) < Integer.parseInt(s2)) {

			pw.println(
					"<br /><br /><strong> Funds transfer can not be initiated as available balance is less than the to be transferred amount <a href=FTWithin.html><i><strong>TRY AGAIN</strong></i></a></strong>");
			remarks = "Funds transfer can not be initiated as available balance is less than the to be transferred amount";
			transtatus = "fail";
		}

		else {

			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");

				pstmt = con.prepareStatement("select * from bankcustomer where act_no=?");

				pstmt.setString(1, s1);

				ResultSet rs = pstmt.executeQuery();

				if (rs.next()) {

					int newRecptBal = Integer.parseInt(rs.getString(8));
					newRecptBal = newRecptBal + Integer.parseInt(s2);
					System.out.println("new recp bal" + newRecptBal);
					pstmt = con.prepareStatement("update bankcustomer set balance=? where act_no=?");
					pstmt.setLong(1, newRecptBal);
					pstmt.setString(2, s1);
					System.out.println("new recp bal" + newRecptBal);
					pstmt.executeUpdate();

					int newBal = Integer.parseInt(c[3].getValue()) - Integer.parseInt(s2);

					pstmt = con.prepareStatement("update bankcustomer set balance=? where act_no=?");

					pstmt.setLong(1, newBal);
					pstmt.setString(2, c[2].getValue());

					pstmt.executeUpdate();

					pw.println("<br /> <br />Funds <strong> rupees " + s2
							+ " </strong> transferred successfully to the account number <strong>" + s1 + "</strong>");
					pw.println("<br /> Transaction Id is : <strong>" + tranid + "</strong>");

					pw.println("<br /><br />Available balance in the account is rupees <strong>" + newBal + "</strong");

					remarks = "Funds transferred successfully";
					System.out.println("New balance is updated successfully in database");
					transtatus = "pass";

				}

				else {

					pw.println("<br /><br /><strong>Recipient account number is incorrect. Please check <a href=FTWithin.html><i><strong>TRY AGAIN</strong></i></a></strong>");
					remarks = "Recipient account number is incorrect";
					transtatus = "fail";
				}

			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} // else

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");

			pstmt = con.prepareStatement(
					"insert into banktransaction(tranid,act_no,tran_desc,tran_status,remarks,ifsc,toact_no,amount) values(?,?,?,?,?,?,?,?)");

			pstmt.setLong(1, tranid);
			pstmt.setString(2, actno);
			pstmt.setString(3, trandesc);
			pstmt.setString(4, transtatus);
			pstmt.setString(5, remarks);
			pstmt.setString(6, s3);
			pstmt.setString(7, s1);
			pstmt.setString(8, s2);
			pstmt.executeUpdate();

			PreparedStatement pstmt1 = con.prepareStatement(
					"insert into banktransactionbackup(tranid,act_no,tran_desc,tran_status,remarks,ifsc,toact_no,amount) values(?,?,?,?,?,?,?,?)");

			pstmt1.setLong(1, tranid);
			pstmt1.setString(2, actno);
			pstmt1.setString(3, trandesc);
			pstmt1.setString(4, transtatus);
			pstmt1.setString(5, remarks);
			pstmt1.setString(6, s3);
			pstmt1.setString(7, s1);
			pstmt1.setString(8, s2);

			pstmt1.executeUpdate();

			System.out.println("Transaction table updated successfully");

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
		} catch (Exception ex) {

			ex.printStackTrace();
		}
		}
	}

}
